<?php

namespace App\Http\Controllers\Api\V1\User;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\ApiController;

class AuthController extends ApiController
{
    public function login(Request $request){
          $request->validate([
            'email'=>'required',
            'password' => 'required',
          ]);
          $device_name = 'web';
          $user = User::where('email', $request->email)->first();
          if ( !$user || !Hash::check($request->password, $user->password)) {
              $response = $this->response('','Incorrect Login Details',401);
              return  $response;
          } else {
              $token = $user->createToken($device_name);
              $arrayToken = (array) $token;
                  $arrayToken['type'] = $user->type;
                  $arrayToken['image'] = $user->image;
                  $arrayToken['email'] = $user->email;
                  $arrayToken['name'] = $user->name;
                  $token = (object) $arrayToken;
          }
          $response = $this->response($token,'success',200);
          return  $response;
    }
    public function updateProfile(Request $request){
        $user = $this->user;
        if($request->has('name') && $user->name != $request->name){
            $user->name = $request->name;
        }
        if($request->has('email') && $user->email != $request->email){
            $request->validate([
                'email' => 'email'
            ]);
            $user->email = $request->email;
        }
        if($request->has('image')) {
            if($user->image != null){
                $path = public_path( $user->image);
                if(File::exists($path)) {
                    unlink($user->image);
                };
            }
            $file =$request->file('image');
            $name = rand(1,10) .  rand(1,10).  rand(1,10) . $file->getClientOriginalName() ;
            $extension= $file->extension();
            $file->storeAs('public/users', $name);
            $user->image = 'storage'.'/'.'users'.'/'. $name;
         }
         $user->save();
         $user = $user->only('id','name','email','image');
         $response = $this->response($user,'success',200);
         return  $response;
    }
    public function create(Request $request){
        $request->validate([
           'warehouse_id' => 'required',
           'name' => 'required',
           'email' => 'required|unique:users',
           'password' => 'required|confirmed',
           'phone' => 'required',
           'address' => 'required',
           'type'=> 'required'
        ]);
        $name= $request->name;
        $email= $request->email;
        $password= Hash::make($request->password);
        $phone= $request->phone;
        $address= $request->address;
        $warehouse_id= $request->warehouse_id;
        $type= $request->type;

        $user = new User;
        $user->name =$name;
        $user->email =$email;
        $user->password =$password;
        $user->phone =$phone;
        $user->address =$address;
        $user->warehouse_id =$warehouse_id;
        $user->type =$type;

        if($user->save()){
        $response = $this->response($user,'success',201);
        return  $response;
        }
        $response = $this->response('','Something went wrong',500);
        return  $response;
    }
    public function getProfile(){
        $user =$this->user;
        $response = $this->response($user,'success',200);
        return  $response;
    }
}

<?php

namespace App\Http\Controllers\Api\V1\Warehouse;

use App\Models\Category;
use App\Models\Warehouse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

class WarehouseController extends ApiController
{
    public function createWarehouse(Request $request){
         $request->validate([
            'name' => 'required',
            'type' => 'required'
         ]);
        $name = $request->name;
        $type = $request->type;
        $warehouse = new Warehouse;
        $warehouse->name = $name;
        $warehouse->type = $type;
        if($warehouse->save()){
            $response = $this->response($warehouse,'added successfully',201);
            return  $response;
        }
            $response = $this->response('','Something went wrong',500);
            return  $response;
    }

    public function index(){
        $warehouses = Warehouse::get();
        $response = $this->response($warehouses,'success',200);
        return  $response;
    }

    public function getCategory(Request $request){
        $warehouse_id = $request->warehouse_id;
        $warehouse= Warehouse::with('categories')->findOrFail($warehouse_id);
        $categories=$warehouse->categories;
        if(count($categories)> 0 ){
            $response = $this->response($categories,'success',200);
            return  $response;
        }
        $response = $this->response('','No categories found',404);
        return  $response;
    }

    
}

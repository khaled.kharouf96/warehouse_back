<?php

namespace App\Http\Controllers\Api\V1\Category;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Models\Warehouse;

class CategoryController extends ApiController
{
    public function createCategory(Request $request){
        $request->validate([
            'warehouse_id' => 'required',
            'name' => 'required'
        ]);
        $warehouse_id = $request->warehouse_id;
        $name = $request->name;
        $category = new Category;
        $category->name = $name;
        $category->warehouse_id = $warehouse_id;
        $warehouse = Warehouse::findOrFail($warehouse_id);
        if( $category->save()){
            $response = $this->response($category,'success',201);
            return  $response;
        }
        $response = $this->response('','Something went wrong',500);
        return  $response;
    }
}

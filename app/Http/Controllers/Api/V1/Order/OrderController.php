<?php

namespace App\Http\Controllers\Api\V1\Order;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;
use App\Models\Order;

class OrderController extends ApiController
{
    public function sell(Request $request){
       $request->validate([
         'product_id' => 'required',
         'quantity' => 'required',
         'type' => 'required',
       ]);
       $user = $this->user;
       $product= Product::find($request->product_id);
       if($product){
            if($product->quantity >= $request->quantity){
                $order = new Order;
                $order->product_id = $request->product_id;
                $order->type = Order::ORDER_SELL;
                $order->quantity= $request->quantity;
                $order->user_id = $user->id;
                $order->total_price= $product->price * $request->quantity;
                if( $order->save()){
                    $product->update([
                       'quantity' => $product->quantity-$request->quantity
                    ]);
                    $response = $this->response($order,'Added Successfully',201);
                    return  $response;
                }
                $response = $this->response('','Something went wrong',500);
                return  $response;
            }
            $response = $this->response('','No products enough for this quantity',404);
            return  $response;
        }
        else{
            $response = $this->response('','No products Found',404);
            return  $response;
        }
    }
}

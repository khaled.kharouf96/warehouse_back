<?php

namespace App\Http\Controllers\Api\V1\Product;

use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\ApiController;

class ProductController extends ApiController
{
   public function index(Request $request){
    $category_id = $request->category_id;
    $category = Category::find($category_id);
    if($category){
        $products = $category->products;

        if(count($products)> 0 ){
            return response()->json($products,200);
        }
        return response()->json('No products found',404);
    }
    return response()->json('No categories found',404);
   }
   public function create(Request $request){
    $request->validate([
        'category_id'=>'required',
        'name' => 'required',
        'quantity' => 'required',
        'price'=> 'required',
    ]);
    $category_id = $request->category_id;
    $category = Category::findOrFail($category_id);
    $product = new Product;
    $product->name = $request->name;
    $product->quantity = $request->quantity;
    $product->price = $request->price;
    $product->category_id = $request->category_id;

    if($product->save()){
        $response = $this->response($product,'Added Successfully',201);
        return  $response;
    }
    $response = $this->response('','Something went wrong',500);
    return  $response;
   }
}

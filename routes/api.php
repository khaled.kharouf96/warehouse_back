<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
//User
Route::group(['prefix'=>'user'], function () {
    Route::post('login','User\AuthController@login');
    Route::group(['middleware'=>'auth:sanctum'], function () {
        Route::post('update-profile','User\AuthController@updateProfile');
        Route::post('create','User\AuthController@create');
        Route::get('profile','User\AuthController@getProfile');
    });
    
  
});

Route::group(['prefix'=>'warehouse','middleware'=>'auth:sanctum'], function () {
    Route::get('index','Warehouse\WarehouseController@index');
    Route::post('create','Warehouse\WarehouseController@createWarehouse');
    Route::get('{warehouse_id?}/categories','Warehouse\WarehouseController@getCategory');
});

Route::group(['prefix'=>'category','middleware'=>'auth:sanctum'], function () {
    Route::get('{category_id?}/products','Product\ProductController@index');
    Route::post('create','Category\CategoryController@createCategory');
});


Route::group(['prefix'=>'product','middleware'=>'auth:sanctum'], function () {
    Route::post('create','Product\ProductController@create');
});

Route::group(['prefix'=>'order','middleware'=>'auth:sanctum'], function () {
    Route::post('sell','Order\OrderController@sell');
});

